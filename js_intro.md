#http://bit.ly/1L2hTpi

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

# Объекты  #


```
#!javascript
var Shape = function(x,y){
        this.x = x, this.y=y;
}
Shape.prototype.move = function(x,y){
    this.x = x, this.y=y;
}
Shape.prototype.draw = function(x,y){
    console.log("x: "+this.x+", y:"+this.y);
}
var point = new Shape(10,20);
point.draw();
//x: 10, y:20
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

















# this #

```
#!javascript
function randomCoordinates(maxVal) {
    this.x = Math.round(Math.random() * maxVal);
    this.y = Math.round(Math.random() * maxVal);
}
```

##call, apply

```
#!javascript
randomCoordinates.call(point,10);
point.draw();
//x: 4, y:2

randomCoordinates.apply(point,[100]);
point.draw();
//x: 43, y:79
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

# Наследование #

```
#!javascript
var Circle = function(x,y,r){
    this.r = r;
    Shape.call(this, x, y);
}
// static
Circle.pi = 3.14;

Circle.prototype = Object.create(Shape.prototype)
Circle.prototype.constructor = Circle;

Circle.prototype.getPerim = function(){
    return 2 * this.r * Circle.pi;
}

Circle.prototype.draw = function(){
    console.log("x: "+this.x+", y:"+this.y+", r:"+this.r);
}

var c = new Circle(1,2,3);
c.draw();
//x: 1, y:2, r:3
c.move(5,6);
c.draw();
//x: 1, y:2, r:3
console.log(c.getPerim())
//18.84
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#inherit and extend
```
#!javascript

function inherit (p) {
    function f(){}
    f.prototype = p;
    return new f();
}


function extend (o, p) {
    for (prop in p){
        o[prop] = p[prop];
    }
    return o;
}
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#Замыкания

```
#!javascript

var i = 10;

var Increment = function(){
    var i = 0;

    return function(){
        return ++i;
    }
}

var inc =  Increment()
console.log(inc());
//1
console.log(inc());
//2
console.log(inc());
//3
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


## Частные функции
```
#!javascript
var Point = function(x,y){
    this.setX = function(newX){
        x = newX;
        return x
    }
    this.getX = function(newX){
        return x
    }
}

var p = new Point()
p.setX(10);
console.log(g.getX());
//10
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


## Singleton just for fun

```
#!javascript
var Singleton = function(){
  var instance = null;
  var F = function(){
          this.i = 100
  }

  return function(){
    if (!instance)
      instance = new F();
    return instance;
  }
}

var singleton = Singleton();
var v1 = singleton(),
    v2 = singleton()

console.log(v1 === v2)
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#Promise
```
#!javascript
var getData = function(req, res){
    db.find({category: req.params.category}, function(err, data){
        if (err)
            return res.json(500, err)

        res.json(200, data)
    });
}
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


```
#!javascript
var getDataByCategory = function(category, onSuccess, onError){
    db.find({category: req.params.category}, function(err, data){
        if (err)
            return onError(err)

        onSuccess(data)
    });
}

var getData = function(req, res){
    getDataByCategory(
        req.params.category,
        function(data){res.json(500, data)},
        function(data){res.json(200, data)},
    )
}
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


```
#!javascript
step1(function (value1) {

    step2(value1, function(value2) {

        step3(value2, function(value3) {

            step4(value3, function(value4) {

                // Do something with value4
            });

        });

    });

});
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#Deferred

Q = require("q");

var getDataByCategory = function(category){
    var defer = Q.defer();
    db.find({category: req.params.category}, function(err, data){
        if (err)
            return defer.reject(err)

        defer.resolve(err)
    });
    return defer.promise
}

var getData = function(req, res){
    getDataByCategory(req.params.category,
        .then(function(data){res.json(500, data)}),
        .fail(function(data){res.json(200, data)})
    )
}

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#Наблюдатель

```
#!javascript
var Callbacks = function(){
    var list = [];
    return {
        add: function(fn){
            list.push(fn);
        },
        remove: function(i){
            list.splice(index,1)
        },
        fire: function(){
            for (var i=0; i<list.length; i++)
               list[i].apply(null, arguments);
        }
    }
}


var c = Callbacks();
c.add(function(val){console.log("first guy says " + val)});
c.add(function(val){console.log("second guy says " + val)});
c.add(function(val){console.log("third guy says " + val)});
c.fire("Yo!");
/*
first guy says Yo!
second guy says Yo!
third guy says Yo!
*/

```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


#Deffered
```
#!javascript
var Deferred = function(){
    var stages = [
        ["resolve", "done", Callbacks()],
        ["reject", "fail", Callbacks()]
    ]

    var promise = {};
    var defer = {}
    stages.forEach(function(stage){
        defer[stage[0]] = stage[2].fire
        promise[stage[1]] = stage[2].add
    });

    defer["promise"] = promise;
    return defer;
}
```

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`

`


```
function getSomething(){
    var defer = Deferred()
    setTimeout(function (){
            defer.resolve("10");
    }, 10000)
    return defer.promise;
}

getSomething()
    .done(function(val){
            console.log(val);
    })
```